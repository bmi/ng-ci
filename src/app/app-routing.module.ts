import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainerComponent } from './mainer/mainer.component';
import { TableSampleComponent } from './table-sample/table-sample.component';
import { TreeSampleComponent } from './tree-sample/tree-sample.component';
import { DashboardSampleComponent } from './dashboard-sample/dashboard-sample.component';
import { DragDropSampleComponent } from './drag-drop-sample/drag-drop-sample.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: MainerComponent },
  { path: 'table', component: TableSampleComponent },
  { path: 'tree', component: TreeSampleComponent },
  { path: 'dash', component: DashboardSampleComponent },
  { path: 'drag', component: DragDropSampleComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
