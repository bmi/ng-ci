import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TableSampleComponent } from './table-sample/table-sample.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { TreeSampleComponent } from './tree-sample/tree-sample.component';
import { MatTreeModule } from '@angular/material/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { DragDropSampleComponent } from './drag-drop-sample/drag-drop-sample.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DashboardSampleComponent } from './dashboard-sample/dashboard-sample.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { LayoutModule } from '@angular/cdk/layout';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { NavComponent } from './nav/nav.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MainerComponent } from './mainer/mainer.component';
import { AppService } from './app.service';

@NgModule({
  declarations: [
    AppComponent,
    TableSampleComponent,
    TreeSampleComponent,
    DragDropSampleComponent,
    DashboardSampleComponent,
    NavComponent,
    MainerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTreeModule,
    MatIconModule,
    DragDropModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatListModule,
    LayoutModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule,
    MatSidenavModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
