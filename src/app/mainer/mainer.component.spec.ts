import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainerComponent } from './mainer.component';

describe('MainerComponent', () => {
  let component: MainerComponent;
  let fixture: ComponentFixture<MainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
