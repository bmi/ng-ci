import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mainer',
  templateUrl: './mainer.component.html',
  styleUrls: ['./mainer.component.scss']
})
export class MainerComponent implements OnInit {

  messages: string[] = [
    'Need update angular.json ("outputPath": "public")',
    'Need update package.json ("buildProd": "ng build --prod --base-href=/your-project-name/")',
    'Need update .gitignore (change /dist to /public)',
    'Added Angular Material',
    'Added Material schematics',
    'Added service worker',
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
